#!/bin/sh

printf "Start microk8s\n"; microk8s start
printf "Check microk8s\n"
if ! microk8s status --wait-ready; then
  MEM_LIMIT=4000; REAL_MEM=$(free -m | grep 'Mem:' | tr -s " " | cut -d' ' -f2)
  DISK_LIMIT=20000; REAL_DISK=$(df -m / | tail -1 | tr -s ' ' | cut -d' ' -f4)
  if [ "$REAL_MEM" -lt "$MEM_LIMIT" ]; then printf "\nAvailable RAM - \e[31mless then 4 Gb\e[0m\n"; fi
  if [ "$REAL_DISK" -lt "$DISK_LIMIT" ]; then printf "Free Disk Space - \e[31mless then 20 Gb\e[0m\n"; fi
  sudo snap install microk8s --classic
  sudo usermod -a -G microk8s "$USER"
  sudo chown -f -R "$USER" "$HOME/.kube"
  printf "\nMake reboot by command: \e[34msudo reboot\e[0m\n"; exit 0
fi

microk8s config > "$HOME/.kube/config"
microk8s enable dashboard dns registry ingress storage helm3 fluentd prometheus

printf '# Auto-generated please do not edit it\n' > url_token
printf "Dashboard url: \e[34mhttps://%s\e[0m\n" "$(microk8s kubectl get services -n kube-system | grep 'kubernetes-dashboard' | tr -s ' ' | cut -d' ' -f3)" >> url_token
printf "Dashboard Token: \033[1m%s\e[0m\n" "$(microk8s kubectl -n kube-system describe secret "$(microk8s kubectl -n kube-system get secret | grep 'default-token' | cut -d' ' -f1)" | grep 'token:' | tr -s ' ' | cut -d' ' -f2)" >> url_token
grep -v '#' url_token
printf "\nIf you want to destroy a cluster run command \033[1mmicrok8s reset --destroy-storage; sudo snap remove --purge microk8s\e[0m\n"
