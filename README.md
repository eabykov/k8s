### MicroK8S on Ubuntu

Exec script: `./install-microk8s.sh`

1. Install and start `microk8s`
2. Enable `helm3`, `dashboard`, `dns`, `registry`, and `ingress` addons
3. Enable `prometheus` addon [Prometheus Operator](https://prometheus.io/docs/introduction/overview/) \
   ![image.jpg](./images/architecture.jpg)
4. Enable `fluentd` addon [Elasticsearch-Fluentd-Kibana](https://www.elastic.co/guide/en/kibana/current/discover.html) \
   ![image.jpg](./images/efk.png)
   > You need to set up Kibana to track whatever you are interested in
5. Get and show Kubernetes Dashboard `url` and `token`
6. Wait for all pods to be in `Running` state by command `watch -n0.5 microk8s kubectl get pods --all-namespaces`
